// add node.js module rc522, fs and nodemailer
var rc522 = require("rc522");
var fs = require('fs');
var fs1 = require('fs');
var nodemailer = require('nodemailer');

console.log('Type  Count');
console.log(':::::::::::');

// Called during RFID tag read
rc522(function(rfidSerialNumber){

	var type;
	var id;
	// asynchronized read of type from DB
	fs.readFile('db/type.json', 'utf8', function (err, data) {
    
    		if (err) throw err;
    		var obj = JSON.parse(data);
    		for(var key in obj) {
                //check whether the tag serial number has the prefix of e.g. Milk
    			if(rfidSerialNumber.indexOf(key) > -1) {
				type = obj[key];
				id = rfidSerialNumber;
				// If serial number has the prefix of any product like Milk or Bread, then reads list of product from DB
				fs1.readFile('db/list.json', 'utf8', function (err, data) {
    
    				var obj1 = JSON.parse(data);
					var already = 0;
					var count = 0;
					
					// Determine whether that tag already exists in the DB
    				for(var key in obj1) {

						if(id.indexOf(key) > -1)
						{
							already = 1;

						}
						if (type.indexOf(obj1[key]) > -1)
						{
							count++;
						}
      						
					}
					// If product exists in the DB
					if (already == 1)
					{
						// We first deletes that tag from DB and enquiry whether count of that 
						// particular product is below 1 or not
						// If count below or equal one we send a notification mail to user
						delete obj1[id];
						var json = JSON.stringify(obj1);
						fs1.writeFile('db/list.json', json, 'utf8', function(err,data){
							count--;
							if (count <= 1)
							{
								// create reusable transporter object using the default SMTP transport
								var transporter = nodemailer.createTransport({
         								host: 'smtp.gmail.com',
    									port: 465,
    									secure: true, // use SSL 
    									auth: {
        								user: 'shifuddin.masud@gmail.com',
        								pass: 'Mcsa@341'
    									}
    								});


								var mailOptions = {
    									from: '"Inventory System" <shifuddin.masud@gmail.com>', // sender address
    									to: 'shifuddin.masud@gmail.com', // list of receivers
    									subject: 'Product shortage', // Subject line
    									text: 'Hello Shifuddin ?', // plaintext body
    									html: '<b>Hello Shifuddin, I found you are running out of ' +type+ ' .Please pay your attention</b> <p>This is automated mail. Please do not reply</p>' // html body
								};

								// send mail with defined transport object
								transporter.sendMail(mailOptions, function(error, info){
    									if(error){
        									return console.log(error);
    									}
    									console.log('Email sent to user for low quantity');
								});
							}
							console.log(type, "  "+count);
						});
					}
					// If product does not exists in the list, we add it
					else
					{
						obj1[id]=type;
						var json = JSON.stringify(obj1);
						fs1.writeFile('db/list.json', json, 'utf8', function(err,data){
						
							// Finally declare the new count after addition
							fs1.readFile('db/list.json', 'utf8', function(err,data){
								var obj2 = JSON.parse(data);
								var count = 0;
								
								for(var key in obj2)
								{
									if (type.indexOf(obj2[key]) > -1)
									{
										count++;
									}				
				
								}
								console.log(type, "  "+count);
							
							});

						});
						
					}

				});
			}
 		}
	});
});
